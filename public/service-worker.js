self.addEventListener('fetch', function(event) {
    if (event.request.method === 'GET' && event.request.url.endsWith('.html')) {
      event.respondWith(
        caches.match(event.request).then(function(response) {
          const promise = fetch(event.request).then(function(networkResponse) {
            // Clone the response to store in the cache
            const clonedResponse = networkResponse.clone();
  
            // Cache the response
            caches.open('pages').then(function(cache) {
              cache.put(event.request, clonedResponse);
            });
  
            return networkResponse;
          });
          return response || promise;
        })
      );
    }
  });
  